# Python Base Image

A container image used for running python projects based on [ansible/python-base-image](https://github.com/ansible/python-base-image).
